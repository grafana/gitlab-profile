# Grafana Working Group | Alarming & Monitoring

The Grafana working group operates to consolidate and share knowledge related to technologies such as Grafana & Prometheus. However, it also provides more general knowledge on Alarming & Monitoring. The information, tutorials, guidelines and templates provided by the working group are constantly evolving depending on the needs of our colleges and stakeholders.

With the increasing complexity of infrastructure having more nodes with many services spread across multiple data centres or facilities, the task of monitoring is becoming increasingly more difficult. Yet, in a time with containerized applications & autoscaling the integration of monitoring functionality can be entirely automated. In these guidelines we will cover applying typical patterns, deriving requirements and much more.

## Index

- [General monitoring guidelines](https://git.astron.nl/groups/grafana/-/wikis/general-monitoring-guidelines)
- [Grafana OAuth Keycloak integration](https://git.astron.nl/groups/grafana/-/wikis/Grafana-OAuth-Keycloak-Integration)
- [Resources](https://git.astron.nl/groups/grafana/-/wikis/resources)
- [Requirements engineering \[WiP\]](https://git.astron.nl/groups/grafana/-/wikis/requirements-engineering)
